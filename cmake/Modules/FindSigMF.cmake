INCLUDE(FindPkgConfig)
PKG_CHECK_MODULES(PC_SIGMF SigMF)

find_path(
    SIGMF_INCLUDE_DIR
    NAMES sigmf/sigmf.h
    PATHS /usr/local/include
          /usr/include
          /usr/local/opt/sigmf/
          $ENV{SIGMF_ROOT}
          ${SIGMF_ROOT}
)

set(SIGMF_FLATBUFFERS_INCLUDE_DIR ${SIGMF_INCLUDE_DIR}/external/)
set(SIGMF_JSON_INCLUDE_DIR ${SIGMF_INCLUDE_DIR}/external/)
set(SIGMF_INCLUDE_DIRS ${SIGMF_INCLUDE_DIR} ${SIGMF_FLATBUFFERS_INCLUDE_DIR} ${SIGMF_JSON_INCLUDE_DIR} ${SIGMF_INCLUDE_GEN_DIR})

INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(SIGMF DEFAULT_MSG SIGMF_INCLUDE_DIR)
MARK_AS_ADVANCED(SIGMF_INCLUDE_DIRS)